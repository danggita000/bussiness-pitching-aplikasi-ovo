## NO 1

**1. USE CASE USER**

| use case | peringkat |
| ------ | ------ |
|Melakukan Registrasi|90|
|Melakukan Login|90|
|Mengganti Password ketika Lupa Password|90
|Melihat Profil|80
|Mengubah Profil|80
|Melakukan Top Up|100|
|Melakukan Transfer|100|
|Melakukan Tarik Tunai|100|
|Menampilkan Histori Transaksi|80|
|Membeli Pulsa|70
|Membeli Paket Data|70
|Melakukan Pembayaran PLN|70
|Melakukan Pembayaran PBB|70
|Melakukan Pembayaran dengan Scan QR Code | 60
|Melakukan Pembayaran dengan Menampilkan QR Code|60
|Melakukan Pembayaran di Indomaret dengan Kode Bayar|80
|Mengirim Laporan Pengaduan kepada Admin|80

**2. USE CASE ADMIN**

| use case | peringkat |
| ------ | ------ |
| Mengakses semua data User |100
| Melakukan perubahan pada user |100
| Menghapus User |100
| Menampilkan Transaksi Berdasarkan User|90
| Menampilkan Transaksi Berdasarkan Waktu Tertentu|90
| Menampilkan seluruh transaksi |100
| Menerima Laporan User |80
| Memberikan Respon terhadap Laporan User |80
| Melakukan Pemblokiran terhadap User |90
| Melakukan analisis terhadap aktivitas User|90 

**3. USE CASE DIREKSI PERUSAHAAN**
| use case | peringkat |
| ------ | ------ |
| Mengakses Data User dan Admin | 100
| Mengakses Data Transaksi | 100
| Melakukan Pengecekan Analisis yang dibuat Admin | 100





## NO 2
https://app.diagrams.net/?src=about#G18tA79tyiBWtIl19OT3ts-nal9VvUTckQ

## NO 3 (untuk kodenya dilampirkan bersama file README)
1. Single Responsibility Principle (Prinsip Tanggung Jawab Tunggal): Komponen-komponen dalam komponen fungsi App(), Login(), Signup(), dan Update(), memiliki masing-masing fungsi yang berbeda. Bisa dikatakan bahwa setiap komponen/method memiliki tanggung jawab tunggal yang berbeda. Misalnya pada program Login dan SignUp yang neniliki function hadleSubmit, disini handleSubmit hanya memiliki 1 fungsi yaitu melakukan handle terhadap submit yang dilakukan user. Jadi program yang dibuat telah memenuhi poin pertama prinsip SOLID.

2. Open-Closed Principle (Prinsip Terbuka-Tertutup): Tidak ada contoh konkret dari prinsip ini dalam program yang dibuat, namun progam dirancang dengan cara yang memungkinkan perluasan fungsionalitas di masa depan.
3. Liskov Substitution Principle (Prinsip Substitusi Liskov): Tidak ada kelas turunan yang terlihat dalam kode tersebut, jadi prinsip ini tidak berlaku dalam konteks ini. Seharusnya kelas turunan ini terdapat pada fitur transaksi, namun karena fitur tersebut tidak terselesaikan, jadi poin SOLID yang ke-3 ini tidak dapat diterapkan.
4. Interface Segregation Principle (Prinsip Pemisahan Antarmuka): Poin ini diterapkan dimana antarmuka login, signup, update atau bahkan menu utama dipisahkan, sehingga kode login kita tidak akan bercampur dengan code Signup.
5. Dependency Inversion Principle (Prinsip Inversi Ketergantungan): Komponen Signup, Login, Updtae, Delete menggunakan axios untuk melakukan permintaan HTTP. Prinsip inversi ketergantungan dapat diterapkan dengan menggunakan injeksi ketergantungan, yaitu dengan menyediakan axios sebagai dependensi eksternal yang diimpor ke dalam komponen Signup. Hal ini memungkinkan fleksibilitas dalam mengganti implementasi HTTP client di masa depan tanpa harus mengubah kode komponen Signup.


## NO 4
Untuk penggunaan design pattern sebenarnya kurang dibutuhkan karena mengingat programnya juga tidak bergitu kompleks. Tapi disini karena terdapat pembuatan objek(user), jadi saya menggunakan design pattern Factory design pattern, dimana design pattern ini bertugas memisahkan logika pembuatan dan penggunaan objek.

Design Pattern ini diterapkan pada Signup, ini [programnya](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/blob/main/frontend/Signup.js)

## NO 5
**Konektivitas Ke Database**

Database yang digunakan adalah SQLite, dengan konektivitas menggunakan library SQLAlchemy yang merupakan perpustakaan python yang memungkinkan kita untuk berinteraksi dengan basis data yang sebelumnya dipilih. Berikut [file database](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/blob/main/database2222.db), dan 
[program koneksi ke database](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/blob/main/backend(web%20service)/database.py).


## NO 6
Web Service yang dibuat menggunakan library fastapi, menggunakan bahasa pemrograman python. 

**Berikut ss dari tampilan web service:**
1. [tampilan awal](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/20a998ee3cdb20702676f94baeb635c3/ss_tampilan_awal.png)
2. [operasi create user](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/e3ef543af0ec117c3ffaa06576d700a3/create_awal.png)

    [hasil operasi create](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/d47c6f8a77ece7981ec14671c325b2dc/create_hasil.png)
3. [operasi read/get semua user](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/46223168f9fc03d2be0d0d8ed312cfda/get_awal.png) (tergantung kita mau ambil dari index berapa sampe berapa)
    
    [hasil operasi read semua user](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/05bdb085668ba816e9d2727ac49f953d/get_hasil.png)

    [operasi read berdasarkan id](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/9a268fb734b9f2bcdb4ff22fa04f9eff/get_user_by_id_awal.png)

    [hasil operasi read berdasarkan id](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/ed3e994617987a875860ea4234aacf93/get_user_by_id_hasil.png)
4. [operasi update/edit user](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/3f4ccf2f6f601d41c4b45fb7f77b60e1/put_awal.png)

    [hasil operasi update ](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/5a7a483eee1c0d56fcd9d2ab4f36f114/put_hasil.png)
5. [operasi delete](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/98dd274c141ed80b88b789af59fe30cd/delete_awal.png)

    [hasil operasi delete](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/78e17bcce0b3fb4022e2cefdc566e2d7/delete_hasil.png)

    bisa dilihat kalo user dengan id 8 telah dihapus : [user id 8](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/211144f1c59b6f85f48d0362e0a58339/user_id_8.png), dan di list pun tidak ada user dengan id 8: [list](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/8edc6e55f9495e5a1dbb2056fc38010e/user_8_gaada.png)

**Berikut link program nya:**
- [program CRUD](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/blob/main/backend(web%20service)/crud.py)
- [program Main](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/blob/main/backend(web%20service)/main.py)

## NO 7
Untuk GUI, saya menggunakan React JS

[Dokumentasi GUI](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/-/issues/5)

penjelasannya di [Youtube](https://youtu.be/j0S_TYK1aV0).


## NO 8
HTTP connection yang saya menggunakan pustaka yang digunakna bersama aplikasi React JS adalah Axios atau Fetch API.

**Berikut SS dari code HTTP Connection:**

- [post(create user)](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/716154890a9e9cfc48318937494d8b81/post_signup.png), disini akan diminta untuk memasukan atribut atribut user yang dibutuhkan, HTTP Connection ini digunakan dalam proses sign up.


- [post(login user)](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/54415a71d7de7a561741dd3758fd2bc0/post_login.png), post disini maksudnya adalah kita melakukan push data bukan dengan tujuan menambah data melainkan melakukan pengecekan data yang diinput dengan data yang ada di database.

- [get(read/mengambil data user)](https://gitlab.com/danggita000/bussiness-pitching-aplikasi-ovo/uploads/398900a9f770b760ae7fedd5a982f4df/get_data.png), disini mungkin lebih ke bagian admin, yang dapat menampilkan semua daftar user dan info nya.

## NO 9
**Berikut link youtubenya:**
https://youtu.be/j0S_TYK1aV0

## NO 10
ga saya kerjain pak maaf












