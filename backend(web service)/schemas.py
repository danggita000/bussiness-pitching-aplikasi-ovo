from pydantic import BaseModel

class UserCreate(BaseModel): 
    name: str
    noHP: str
    email: str
    balance: int
    password: str

class UserLogin(BaseModel):
    noHP: str
    password: str

class User(BaseModel):
    id: int
    name: str
    noHP: str
    email: str
    balance: int
    password: str


    class Config:
        orm_mode = True
        