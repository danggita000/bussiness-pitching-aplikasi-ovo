import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Validation from './LoginValidation'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Login(){
    
    const [inputData, setInputData] = useState({
        noHP:'',
        password:''
    })

    // const handleInput = async (event) => {
    //     setValues(prev => ({...prev, [event.target.name]: [event.target.value]}))
    // }

    const [errors, setErrors] = useState({})

    const navigat = useNavigate();
    
    function handleSubmit (event){
        event.preventDefault(); 
        setErrors(Validation(inputData));

        axios.post('http://127.0.0.1:8000/login/', inputData)
        .then(res => {
            alert("Sign In Successfuly!")
            navigat('/home')
        }).catch(err => console.log(err))
    }

    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', backgroundColor: '#9B59B6' }}>
          <div style={{ backgroundColor: 'white', padding: '3vw', borderRadius: '0.5vw', width: '25vw' }}>
            <form onSubmit={handleSubmit}>
              <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
                <h2 style={{ marginBottom: '2vw', textAlign: 'center', fontSize: '2.3vw', marginTop: '0vw' }}>Welcome Back</h2>
                <label htmlFor="noHP"><strong>Phone Number</strong></label>
                <input type="text" placeholder='Enter Phone Number' name='noHP'
                  onChange={e => setInputData({ ...inputData, noHP: e.target.value })} />
                {errors.noHP && <span className='text-danger'>{errors.noHP}</span>}
              </div>
              <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
                <label htmlFor="password"><strong>Password</strong></label>
                <input type="text" placeholder="Enter Password" name='password'
                  onChange={e => setInputData({ ...inputData, password: e.target.value })} />
                {errors.password && <span className='text-danger'>{errors.password}</span>}
              </div>
              <button type='submit' style={{ backgroundColor: '#9B59B6', width: '100%', borderRadius: '0', color: 'white' }}>Sign in</button>
              <p style={{ fontSize: '1vw' }}>Didn't have an account? <Link to='/signup'>Create Account</Link></p>
            </form>
          </div>
        </div>
      );      
}

export default Login