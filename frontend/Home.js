import React from 'react';


const navbarStyle = {
  backgroundColor: '#9B59B6',
  color: 'white',
  padding: '10px',
};

class Navbar extends React.Component {
  render() {
    return (
      <nav style={navbarStyle}>
        <ul>
          <li>
            <a href="/">Beranda</a>
          </li>
          <li>
            <a href="/profil">Profil</a>
          </li>
          <li>
            <a href="/produk">Produk</a>
          </li>
          <li>
            <a href="/kontak">Kontak</a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
