import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import App from './App'
import Signup from './Signup';
import Login from './Login';
import Home from './Home';

function AppRouter(){
    return(
        <BrowserRouter>
            <Routes>
                <Route path='/user' element={<App/>}/>
                <Route path='/signup' element={<Signup/>}/>
                <Route path='/' element={<Login/>}></Route>
                <Route path='/home' element={<Home/>}></Route>
            </Routes>
        </BrowserRouter>
    );
}

export default AppRouter