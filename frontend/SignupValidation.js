function Validation(values){
    let error = {}
    const noHP_pattern = /^\d{10,12}$/
    const name_pattern = /^[A-Za-z\s]+$/
    const email_pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    // const password_pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-0]{8,}$/

    if(values.email ===""){
        error.email = "Email should not be empty"
    }
    else if(!email_pattern.test(values.email)){
        error.email = "Email Didn't match"
    } else {
        error.email = ""
    }

    if(values.name ===""){
        error.name = "Name should not be empty"
    }
    else if(!name_pattern.test(values.name)){
        error.name = "Name Didn't match"
    } else {
        error.name = ""
    }

    if(values.noHP ===""){
        error.noHP = "Phone Number should not be empty"
    }
    else if(!noHP_pattern.test(values.noHP)){
        error.noHP = "Phone Number Didn't match"
    } else {
        error.noHP = ""
    }

    if(values.password === ""){
        error.password = "Password should not be empty"
    }
    // else if(!password_pattern.test(values.password)){
    //     error.password = "Password didn't match"
    // } 
    else {
        error.password = ""
    }
    return error;
}

export default Validation