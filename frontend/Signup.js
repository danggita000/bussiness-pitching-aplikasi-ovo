import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Validation from './SignupValidation';
import axios from 'axios'
import { useNavigate } from 'react-router-dom';

// Factory function untuk membuat instance komponen Signup
function SignupFactory() {
  const [inputData, setInputData] = useState({
    noHP: '',
    name: '',
    email: '',
    balance: 0,
    password: ''
  });

  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();
    setErrors(Validation(inputData));

    axios.post('http://127.0.0.1:8000/users/', inputData)
      .then(res => {
        alert("Data Added Successfully!");
        navigate('/');
      })
      .catch(err => console.log(err));
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', backgroundColor: '#9B59B6' }}>
      <div style={{ backgroundColor: 'white', padding: '3vw', borderRadius: '0.5vw', width: '25vw' }}>
        <form onSubmit={handleSubmit}>
          <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
            <h2 style={{ marginBottom: '2.5vw', marginTop: '0px', textAlign: 'center', fontSize: '2.3vw' }}>Sign Up</h2>
            <label htmlFor="name"><strong>Name</strong></label>
            <input type="text" id="name" placeholder="Enter Name" name="name" onChange={e => setInputData({ ...inputData, name: e.target.value })} />
            {errors.name && <span className='text-danger'>{errors.name}</span>}
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
            <label htmlFor="noHP"><strong>Phone Number</strong></label>
            <input type="text" id="noHP" placeholder="Enter Phone Number" name="noHP" onChange={e => setInputData({ ...inputData, noHP: e.target.value })} />
            {errors.noHP && <span className='text-danger'>{errors.noHP}</span>}
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
            <label htmlFor="email"><strong>Email</strong></label>
            <input type="email" id="email" placeholder="Enter email" name="email" onChange={e => setInputData({ ...inputData, email: e.target.value })} />
            {errors.email && <span className='text-danger'>{errors.email}</span>}
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1vw' }}>
            <label htmlFor="hashed_password"><strong>Password</strong></label>
            <input type="password" id="hashed_password" placeholder="Enter Password" name="hashed_password" onChange={e => setInputData({ ...inputData, password: e.target.value })} />
            {errors.password && <span className='text-danger'>{errors.password}</span>}
          </div>
          <button type='submit' style={{ backgroundColor: '#9B59B6', width: '100%', borderRadius: '0', color: 'white' }}>Sign Up</button>
          <p style={{ fontSize: '0.9vw' }}>Already have an account? <Link to="/">Sign in</Link></p>

        </form>
      </div>
    </div>
  );
}

// Export instance komponen Signup dari factory function
const Signup = SignupFactory();
export default Signup;
