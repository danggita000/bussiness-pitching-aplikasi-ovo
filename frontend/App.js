import axios from 'axios'
import React, {useEffect, useState} from 'react'
import { Link } from 'react-router-dom'
// import Validation from './LoginValidation'

function App(){
  const [columns, setColumns] = useState([])
  const [records, setRecords] = useState([])

  useEffect(()=> {
    axios.get('http://127.0.0.1:8000/users/')
    .then(res=>{
      setColumns(Object.keys(res.data[0]))
      setRecords(res.data)
    })
  }, [])

  return(
    <div className="container mt-15">
      <div className="text-end"><Link to="/signup" className="btn btn-primary">Add</Link></div>
      <table className="table" >
        <thead>
          <tr>
          {columns.map((c,i) => (
            <th key={i}>{c}</th>
          ))}
          </tr>
        </thead>
        <tbody>
          {
            records.map((d, i) => (
              <tr key={i}>
                <td>{d.noHP}</td>
                <td>{d.id}</td>
                <td>{d.name}</td>
                <td>{d.email}</td>
                <td>{d.balance}</td>
                <td>{d.password}</td>                
              </tr>
            ))
          }
          </tbody>
      </table>
    </div>
  )
}

export default App