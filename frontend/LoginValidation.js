function Validation(values){
    let error = {}
    const noHP_pattern = /^\d{10,12}$/

    if(values.noHP ===""){
        error.noHP = "Phone Number should not be empty"
    }
    else if(!noHP_pattern.test(values.noHP)){
        error.noHP = "Phone Number Didn't match"
    } else {
        error.noHP = ""
    }

    if(values.password === ""){
        error.password = "Password should not be empty"
    }
    else {
        error.password = ""
    }
    return error;
}

export default Validation